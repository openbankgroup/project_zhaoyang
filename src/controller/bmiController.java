package controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class bmiController {
	@RequestMapping("/test")
	public String computeBMI(@RequestParam("height") double height, @RequestParam("weight") double weight,
			HttpServletRequest request) {
		double bmi;
		System.out.println("height:"+height);
		System.out.println("weight:"+weight);
		bmi=height/Math.pow(weight, 2);
		return "index";
	}
}
